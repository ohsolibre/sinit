CC = cc
CFLAGS = -Wextra -Wall -Os
LDFLAGS = -s -static

all: sinit
sinit: sinit.c
	$(CC) -o $@ $^ $(CFLAGS) $(LDFLAGS) 

clean:
	rm -f sinit

.PHONY: all clean
