#define _GNU_SOURCE
#include <unistd.h>
#include <sys/wait.h>
#include <sys/reboot.h>

static sigset_t set;
static char *const initcmd[]= {"/sbin/init", NULL};
static char *const cadcmd[] = {"/sbin/ctrl-alt-del", NULL};

static void spawn(char *const argv[])
{
	if (fork() == 0) {
		sigprocmask(SIG_UNBLOCK, &set, NULL);
		setsid();
		execv(argv[0], argv);
		_exit(1);
	}
}

int main(void)
{
	int sig;
	struct timespec ts = {30, 0};

	if (getpid() != 1)
		return 1;

	chdir("/");
	sigfillset(&set);
	sigprocmask(SIG_BLOCK, &set, NULL);
	reboot(RB_DISABLE_CAD);
	spawn(initcmd);

	for (;;) {
		sig = sigtimedwait(&set, NULL, &ts);
		switch (sig) {
		case -1:
		case SIGCHLD:
			while (waitpid(-1, NULL, WNOHANG) > 0);
			break;
		case SIGUSR1:
		case SIGUSR2:
			sync();
			reboot(sig == SIGUSR1 ? RB_AUTOBOOT : RB_POWER_OFF);
			break;
		case SIGINT:
			spawn(cadcmd);
			break;
		}
	}
}
